package com.afs.restapi.dto;

public class CompanyUpdate {
    private String name;

    public CompanyUpdate() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
