package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.afs.restapi.mapper.EmployeeMapper.toEntity;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAllByStatusTrue();
    }

    public Employee update(int id, Employee toUpdate) {
        Employee employee = findById(id);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return employee;
    }

    public Employee findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList();
    }

    public Employee insert(EmployeeRequest employeeRequest) {
        Employee employee = toEntity(employeeRequest);
        return employeeRepository.save(employee);
    }

    public void delete(int id) {
        Employee employee = findById(id);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
